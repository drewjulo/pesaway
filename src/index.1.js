import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import './pesawidget.css';
import NumberFormat from 'react-number-format';
import Cards from 'react-credit-cards';
import 'react-credit-cards/es/styles-compiled.css';
import ReactPolling from 'react-polling';
import LoadingOverlay from 'react-loading-overlay';
import FadeLoader from 'react-spinners/BounceLoader';
import Swal from 'sweetalert2';

const axios = require('axios');

// const ControlledPopup = () => {
//   const [open, setOpen] = useState(false);
//   const closeModal = () => setOpen(false);
// };

class Pesawidget extends React.Component {
  constructor(props){
      super(props)
  
      this.state = {
        cvc: '',
        expiry: '',
        focus: '',
        name: '',
        number: '',
        payment_option: 'card',
        p_option: 'card',
        first_number: '',
        disp: 'none',
        mobile: '',
        loading: false
      }
      // this.payments = this.payments.bind(this);
      this.limit = this.limit.bind(this);
      this.cardExpiry = this.cardExpiry.bind(this);
      this.cardPayment = this.cardPayment.bind(this);
      this.mobilePayment = this.mobilePayment.bind(this);
      this.onSubmitCardPayment = this.onSubmitCardPayment.bind(this);
      this.onSubmitMobilePayment = this.onSubmitMobilePayment.bind(this);
      // this.handlePaymentOptionChange = this.handlePaymentOptionChange.bind(this);
  }
  
  onSubmitCardPayment(){
    const pw = document.getElementById("pesa_widget");
    const amount = pw.getAttribute("data-amount");
    const apiUsername = pw.getAttribute("data-api-username");
    const currency = pw.getAttribute("data-currency");
    const reference = pw.getAttribute("data-reference");
    const token = pw.getAttribute("data-token");
    const metadata = pw.getAttribute("data-metadata");
    const axios = require('axios');
    // this.setState({loading: true});
    // new part
    axios.post('https://pesamoni.com/api/live/v1/widgets', {
        amount: amount,
        apiUsername: apiUsername,
        currency: currency,
        metadata: metadata,
        payment_method: 'card',
        name: this.state.name,
        cardNumber: this.state.number,
        exp: this.state.expiry,
        cvv: this.state.cvc,
        reference: reference,
        token: token
      })
      .then(function (response) {
        // this.setState({loading: false});
        console.log(response.data);
        Swal.fire({
            html: <iframe srcDoc={response.data} />
            
          })
        // ReactDOM.render(<div dangerouslySetInnerHTML={{ __html: response.data }} />, document.getElementById('root'))
        return 
        // if(response.data.status === true){
        //   // notify();
        //   SweetAlert.fire(
        //     'Great!',
        //     'Your transaction is being confirmed!',
        //     'success'
        //   )
        //   this.setState({loading: false});
        // }
      })
      .catch(function (error) {
        console.log(error);
    });
  }
  
  
  onSubmitMobilePayment(){
    
    const pw = document.getElementById("pesa_widget");
    const amount = pw.getAttribute("data-amount");
    const apiUsername = pw.getAttribute("data-api-username");
    const currency = pw.getAttribute("data-currency");
    const reference = pw.getAttribute("data-reference");
    const token = pw.getAttribute("data-token");
    const metadata = pw.getAttribute("data-metadata");
    
    const axios = require('axios');
    // this.setState({loading: true});
    // new part
    axios.post('https://pesamoni.com/api/live/v1/widgets', {
        amount: amount,
        apiUsername: apiUsername,
        currency: currency,
        metadata: metadata,
        payment_method: 'mm',
        mobile: this.state.mobile,
        reference: reference,
        token: token
      })
      .then(function (response) {
        
        if(response.data.status === true){
          // notify();
         
        
        }
      })
      .catch(function (error) {
        console.log(error);
    });
    
    const fetchData = () => {
        // return a promise
        // return axios.get(callback);
    }
    return (
    <ReactPolling
      url={'url to poll'}
      interval= {3000} // in milliseconds(ms)
      retryCount={3} // this is optional
      onSuccess={() => console.log('handle success')}
      onFailure={() => console.log('handle failure')} // this is optional
      promise={fetchData} // custom api calling function that should return a promise
      render={({ startPolling, stopPolling, isPolling }) => {
        if(isPolling) {
          return (
            <div> Hello I am polling</div>
          );
        } else {
          return (
            <div> Hello I stopped polling</div>
          );
        }
      }}
    />
  );
  }
  
  mobilePayment(){
    return (
      <div className='p_payment_container_mobile'>
        <div className='p_left'>
            <div className='p_left_content'>
              After clicking make payment <br/>Check your phone for prompt <br/>Enter pin to complete transaction
            </div>
              
        </div>
            <div className='p_right'>
              <div className='p_labels'>Enter Mobile</div>
                <NumberFormat className='p_input' name='mobile' format="+256 (###) ######" placeholder="Enter Your Mobile Number" allowLeadingZeros={false} allowNegative={false} type='tel' allowEmptyFormatting mask="_" required onValueChange={(values) => {
                      const {formattedValue, value} = values;
                      if(value[0] === '0'){
                        this.setState({first_number: '0', disp: 'block'})
                      }else{
                        this.setState({first_number: '', disp: 'hidden'})
                      }
                      if(value.length === 9){
                        console.log('lenght is correct');
                        console.log(value);
                        this.setState({mobile: value});
                      }
                }}/>
              
              <button className='p_submit_button' onClick={this.onSubmitMobilePayment}>Make Payment</button>
        </div>
      </div>
    )
  }
  
  cardPayment(){
    console.log(this.state.cvc);
    return (
       <div className='p_payment_container'>
            <div className='p_left'>
              <Cards
                cvc={this.state.cvc}
                expiry={this.state.expiry}
                focused={this.state.focus}
                name={this.state.name}
                number={this.state.number}
              />
            </div>
            <div className='p_right'>
              <div className='p_labels'>Name on Card</div>
              <input className='p_input' name='name' type='text' onChange={this.handleInputChange} onFocus={this.handleInputFocus} placeholder='Enter name on card'/>
              <div className='p_labels'>Card Number</div>
              <input className='p_input'
                type="tel"
                name="number"
                placeholder="Enter Card Number"
                onChange={this.handleInputChange}
                onFocus={this.handleInputFocus}
              />
              <div className='p_equal_divider'>
                <div className='left_side'>
                  <div className='p_labels'>Expiry</div>
                  <NumberFormat className='p_input' placeholder='enter expiry date on card' name='expiry' format={this.cardExpiry} onChange={this.handleInputChange} onFocus={this.handleInputFocus}/>
                </div>
                <div className='right_side'>
                  <div className='p_labels'>Cvv</div>
                  <NumberFormat format="###" placeholder='Enter CVV' className='p_input' name='cvc' onChange={this.handleInputChange} onFocus={this.handleInputFocus} />
                </div>
              </div>
              <button className='p_submit_button' onClick={this.onSubmitCardPayment}>Make Payment</button>
            </div>
            
        </div>
    )
  }
  
  handlePaymentOptionChange =(e) => {
    this.setState({p_option: e.target.getAttribute('name')});
  }
  
  handleInputFocus = (e) => {
    this.setState({ focus: e.target.name });
  }
  
  handleInputChange = (e) => {
    const { name, value } = e.target;
    
    this.setState({ [name]: value });
  }
  
limit(val, max) {
      if (val.length === 1 && val[0] > max[0]) {
        val = '0' + val;
      }
     
      if (val.length === 2) {
        if (Number(val) === 0) {
          val = '01';
     
        //this can happen when user paste number
      } else if (val > max) {
          val = max;
        }
      }
     
      return val;
}
cardExpiry(val) {
        let month = this.limit(val.substring(0, 2), '12');
        let year = val.substring(2, 4);
       
        return month + (year.length ? '/' + year : '');
  
}
  
  render() {
    const pw = document.getElementById("pesa_widget");
    const amount = pw.getAttribute("data-amount");
    const customCharge = pw.getAttribute("data-custom-charge");
    const apiUsername = pw.getAttribute("data-api-username");
    const currency = pw.getAttribute("data-currency");
    
    return (
      
      
        <LoadingOverlay
          active={this.state.loading}
          spinner={<FadeLoader height={15} width={5} radius={2} margin={2} />}
        >
        <React.Fragment>
        <div className='p_container'>
          <div className='p_labels'>Total Amount</div>
          <div className='p_value'><NumberFormat value={amount} displayType={'text'} prefix={currency} thousandSeparator={true} /></div>
          <div className='p_labels'>Payment Method</div>
          <div className='p_scrollable-buttons'>
              <div className={this.state.p_option == 'card' ? 'p_button_active' : 'p_button_inactive'} name='card' onClick={this.handlePaymentOptionChange}>Visa/MasterCard <i className='picon'></i></div>
              <div className={this.state.p_option == 'mm' ? 'p_button_active' : 'p_button_inactive'} name='mm' onClick={this.handlePaymentOptionChange}>Mobile Money <i className='picon'></i></div>
              
          </div>
          
           {this.state.p_option == 'card' ? this.cardPayment() : this.mobilePayment()}
         
          <div className='p_credits'>Payments Supported by Logo</div>
        </div>
        </React.Fragment>
        </LoadingOverlay>
        
      
    );
    
  }

}

const pw = document.getElementById("pesa_widget");
const apiUsername = pw.getAttribute("data-api-username");

    axios.post('https://pesamoni.com/api/live/v1/widgets', {
        apiUsername: apiUsername
      })
      .then(function (response) {
        if(response.data.status === 'success'){
          ReactDOM.render(<Pesawidget/>, document.getElementById('pesa_widget'));
          return;
        }else{
          const element = React.createElement(
            "div",
            {style:{color:"red"}, id: "Error", className: "Error"},
            response.data.description,
          );
          ReactDOM.render(element, document.body.appendChild(document.createElement("DIV")));
          // ReactDOM.render(response.body, document.body.appendChild(document.createElement("DIV")));
          return;
        }
      })
      .catch(function (error) {
        console.log(error);
});


// ReactDOM.render(<Pesawidget/>, document.getElementById('pesa_widget'));